package eu.tncy;

import java.util.ArrayList;

import net.tncy.book;


public class bookstore {

    public ArrayList<book> listeLivre;

    public bookstore() {
        this.listeLivre = new ArrayList<>();
    }

    public void addBook(book b){
        if ( !listeLivre.contains(b)){
            listeLivre.add(b);
        }else{
            System.out.println("Le livre est déjà dans la liste wesh");
        }
    }

    public void removeBook(book b){
        if (listeLivre.contains(b)){
            listeLivre.remove(b);
        }else{
            System.out.println("Il n'y a pas ce livre dans liste.");
        }
    }

    public void findAuthor(String name){
        
        for (book b : listeLivre){
            if (b.getName()==name){
                System.out.println("L'auteur du livre "+name+" est "+ b.getAuteur());
            }
        }
        System.out.println("Le livre "+name+" n'est pas dans la liste.");
    }

    public void findName(String author){
        ArrayList<String> books = new ArrayList<>();

        for(book b : listeLivre){
            if(b.getAuteur()==author){
                books.add(b.getName());
            }
        }

        if (books.size() != 0){
            System.out.println("Le(s) livre(s) de "+author+" est/sont:");
            for(String s : books){
                System.out.println(s);
            }
        }else{
            System.out.println("Il n'y a pas de livre écrit par "+author+" dans la liste.");
        }

    }

    
}