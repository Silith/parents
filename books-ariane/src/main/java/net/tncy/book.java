package net.tncy;

import net.tncy.validator.constraints.books.*;

public class book
{
    private String name;
    private String auteur;
    @ISBN
    private ISBN i;

    public book(String name, String auteur, ISBN isbn) {
        this.name = name;
        this.auteur = auteur;
        this.i = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public ISBN getI() {
        return i;
    }

    public void setI(ISBN isbn) {
        this.i = isbn;
    }
}